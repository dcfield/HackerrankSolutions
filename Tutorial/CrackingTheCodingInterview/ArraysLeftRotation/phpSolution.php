<?php

// Actions to take in the data
$handle = fopen ("php://stdin","r");
fscanf($handle,"%d %d",$n,$d);
$a_temp = fgets($handle);
$a = explode(" ",$a_temp);
array_walk($a,'intval');

// needed variables
$num_rotations = $d;
$array_len = $n;
    
// this will be our final array
$rotated_array = array();
    
// loop through given array
foreach($a AS $key => $value){
    
    // let's move each value left by the number of rotations
    $new_position = $key - $num_rotations;
    
    // some values will be negative.
    // if they are negative,
    // circle them around onto the array
    if($new_position < 0){
        $new_position = $new_position + $array_len;
    }

    // store our value in the new position
    $rotated_array[$new_position] = $value;
}

// PHP does not protect the position of its arrays.
// Needs to be sorted first
ksort($rotated_array);

// Print answer
print_array($rotated_array);

// Function to print out array
function print_array($array){
    foreach($array AS $val){
        echo $val . ' ';
    }
}